<?php
/* 必要な引数を確認 */
if (!isset($_GET["id"])) {
  header("Location: /");
}
/* SQLを実行 */
try {
  $pdo = new PDO("sqlite:../ryusei/CMP.sqlite");
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
  $st = $pdo->prepare("SELECT code FROM pdeStore WHERE id = :id AND published = 1");
  $st->bindValue(":id", $_GET["id"], PDO::PARAM_STR);
  $st->execute();
  $data = $st->fetch(PDO::FETCH_ASSOC);
  if (!$data) header("Location: /");
} catch (PDOException $err) {
  unset($pdo);
  header("Location: /");
}
$code = $data["code"];
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset='utf-8'>
    <title>Processing_Test</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <script src="./js/jquery-2.1.1.min.js"></script>
    <script src="./js/processing.origin.js"></script>
    <link rel="stylesheet" href="./css/material-design-iconic-font.css">
    <link rel="stylesheet" href="./css/debug.css">

    <script>
      var conn = null;
      var id = <?php
        echo json_encode(isset($_REQUEST['id']) ? $_REQUEST['id'] : "", JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS | JSON_HEX_QUOT);
      ?>;

      var showError = function(msg) {
        var errLog = $('textarea#error');
        errLog.scrollTop(errLog[0].scrollHeight - errLog.height());
        errLog.val(errLog.val() + msg + "\n").show();
      };

      console.error = showError;

      var runCode = function(code, type) {
        $('canvas').css({"background-color": "#CCCCCC"});
        try {
          var processing = new Processing($('canvas')[0], code);
        } catch (e) {
          printStackTrace(e);
        }
      };

      function printStackTrace(e) {
        if (e.stack) {
          console.error(e.stack);
        }
        else console.error(e.message, e);
      };

      $(function(){
        var code = <?php echo json_encode($code, JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS | JSON_HEX_QUOT); ?>;
        runCode(code, 'init');
      });

      var docWindow = document.documentElement;
      var displayWidth = docWindow.clientWidth;
      var displayHeight = docWindow.clientHeight;
    </script>
  </head>
  <body>
    <canvas></canvas>
    <textarea id="error" readonly></textarea>
  </body>
</html>
