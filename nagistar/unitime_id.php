<?php
//62進数の設定とUnixタイムの取得
$char = array_merge(range('0','9'), range('a', 'z'), range('A', 'Z'));
$time = microtime(true)*1000;

//62進数化と乱数の付与
$unitime = (encode($time, $char)).rand(0,9);

//jsonに格納
header("Content-Type: application/json");
echo json_encode(array(
  "qr_id" => $unitime,
  "hash" => hash_hmac('sha256', "vanilla".$unitime."salt", false)
));
exit();

//62進数用の関数
function encode($number, $char){
    $result = "";
    $base = count($char);

    while($number > 0){
        $result = $char[ fmod($number, $base) ] . $result;
        $number = floor($number / $base);
    }
    return ($result === "" ) ? 0 : $result;
}

?>
